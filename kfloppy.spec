%undefine __cmake_in_source_build
Name:    kfloppy
Summary: Floppy formatting tool 
Version: 23.04.3
Release: 1

License: GPLv2+
URL:     http://utils.kde.org/projects/%{name}

%global revision %(echo %{version} | cut -d. -f3)
%if %{revision} >= 50
%global stable unstable
%else
%global stable stable
%endif
Source0: http://download.kde.org/%{stable}/release-service/%{version}/src/%{name}-%{version}.tar.xz

BuildRequires: desktop-file-utils
BuildRequires: extra-cmake-modules
BuildRequires: kf5-rpm-macros
BuildRequires: cmake(KF5I18n)
BuildRequires: cmake(KF5CoreAddons)
BuildRequires: cmake(KF5KIO)
BuildRequires: cmake(KF5DocTools)
BuildRequires: cmake(KF5KDELibs4Support)
BuildRequires: cmake(KF5XmlGui)

# translations moved here
Conflicts: kde-l10n < 17.03

# when split occured
Conflicts: kdeutils-common < 6:4.7.80

Obsoletes: kdeutils-kfloppy < 6:4.7.80
Provides:  kdeutils-kfloppy = 6:%{version}-%{release}


%description
KFloppy is a utility that provides a straightforward graphical means
to format 3.5" and 5.25" floppy disks.


%prep
%autosetup


%build
%{cmake_kf5}
%make_build


%install
%make_install

%find_lang %{name} --all-name --with-html


%check
desktop-file-validate %{buildroot}%{_kf5_datadir}/applications/org.kde.kfloppy.desktop


%files -f %{name}.lang
%license COPYING
%doc README
%{_kf5_bindir}/kfloppy
%{_kf5_datadir}/qlogging-categories5/%{name}*
%{_kf5_metainfodir}/org.kde.kfloppy.appdata.xml
%{_kf5_datadir}/applications/org.kde.kfloppy.desktop
%{_kf5_datadir}/icons/hicolor/*/apps/kfloppy.*


%changelog
* Thu Mar 21 2024 peijiankang <peijiankang@kylinos.cn> - 23.04.3-1
- update verison to 23.04.3

* Mon Dec 11 2023 michael_fengjun <fengjun@kylinsec.com.cn> - 22.12.1-1
- Package init
